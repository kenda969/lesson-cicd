import Vue from 'vue'
import Router from 'vue-router'
import  Authentification from '@/views/Authentification'
import  Provision from '@/views/Provision'
import  AddEntry from '@/views/AddEntry'
import  EditEntry from '@/views/EditEntry'

Vue.use(Router);

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'Authentification',
      component: Authentification,
      meta: {
        requiresAuth: false,
      }
    },
    {
      path: '/provision',
      name: 'Provision',
      component: Provision,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/provision/addentry',
      name: 'AddEntry',
      component: AddEntry,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/provision/:id',
      name: 'EditEntry',
      component: EditEntry,
      meta: {
        requiresAuth: true,
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!localStorage.getItem('usertoken')){
      next({
        name: 'Authentification',
        params: { nextUrl: to.fullPath }
      })
    } else next();

  } else next();
});

export default router
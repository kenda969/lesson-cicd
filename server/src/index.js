const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const mongoose = require('mongoose');
const config = require('../config/config');
mongoose.Promise = global.Promise;

const app = express();
const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', '*');
  res.header('Access-Control-Allow-Headers', '*');
  next();
};

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());
app.use(allowCrossDomain);
app.use(bodyParser.urlencoded({extended: false}));
app.use(require('./routes/posts'));
app.use(require('./routes/users'));
app.use(require('./routes/cfg'));

mongoose.connect(config.dbURL, config.dbOptions);
mongoose.connection
  .once('open', () => {
    console.log(`Mongoose - successful connection ...`);
    app.listen(process.env.PORT || config.dbport,
      () => console.log(`Server is listening on port ${config.dbport} ...`))
  })
  .on('error', error => console.warn(error));

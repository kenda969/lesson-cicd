const express = require('express');
const router = express.Router();
const  cors = require("cors");
const  jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const Users = require('../models/user_model');
router.use(cors());
process.env.SECRET_KEY = 'secret';

router.post('/users', (req, res) =>{
  const userData = {
    login: req.body.login,
    password: req.body.password
  };

  Users.findOne({
    login: req.body.login
  })
    .then(user => {
      if (!user){
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          userData.password = hash;
          Users.create(userData)
          .then(user => {
            res.json({status: user.login + ' registred'})
          })
            .catch(err => {
                res.send('error:' + err)
            })
          })
      } else {
        res.json({error: 'User already exists'})
      }
      })
      .catch(err =>{
        res.send('error:' + err)
      });
});

router.post('/login', (req, res) =>{
  Users.findOne({
    login: req.body.login
  })
  .then(user => {
    if (user) {
      if(bcrypt.compareSync(req.body.password, user.password)) {
        const  payload = {
          _id: user._id,
          login: user.login
        };
        let token = jwt.sign(payload, process.env.SECRET_KEY, {
          expiresIn: 1440
        });
        res.send(token)
      } else {
          res.json({error: 'User not exist'})
      }
    } else  {
      res.json({error: 'User not exist'})
    }
  })
  .catch(err => {
    res.send('error:' + err)
  })
})
module.exports = router;
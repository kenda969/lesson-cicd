const { create } = require('xmlbuilder2');
const configgxp = require('../../config/config_gxp')
const Conf = require('../models/conf-model');
const express = require('express');
const router = express.Router();

router.all('/cfg([0-9A-z]{12}).xml', (req, res, next) => {
    const ip_mac = req.url.slice(4,16);

    Conf.findOne({mac: ip_mac}, (err, conf) => {
        if (conf !== null) {
            if (err) {
                res.sendStatus(500)
            } else if (conf.model === 'DP715' ) {
                const DP715 = create({version: '1.0', encoding: 'UTF-8'})
                // DP715
                    .ele('gs_provision', {'version': '1'})
                    .ele({'mac': conf.mac}).up()
                    .ele('config', {'version': '1'})
                    .ele({'P2': configgxp.passPhone}).up()
                    .ele({'P192': configgxp.servURL}).up()
                    .ele({'P237': configgxp.servURL}).up()
                    .ele({'P194': '3'}).up()
                    .ele({'P193': '60'}).up()
                    .ele({'P212': '1'}).up()
                    .ele({'P64': configgxp.timeZone}).up()
                    .ele({'P30': configgxp.serverTimeZone}).up()
                    // Profile #1
                    .ele({'P271': '1'}).up()
                    .ele({'P47': conf.sip_server}).up()
                    .ele({'P63': '0'}).up()
                    .ele({'P81': '0'}).up()
                    .ele({'P31': '0'}).up()
                    .ele({'P4120': conf.name1}).up()
                    .ele({'P4060': conf.login}).up()
                    .ele({'P4090': conf.login}).up()
                    .ele({'P4120': conf.pass}).up()
                    .up()
                    .up();

                const xmlDP715 = DP715.end({prettyPrint: true});
                res.send(xmlDP715)

            } else if (conf.model === 'GXP1610'){

                const GXP1610 = create({ version: '1.0', encoding: 'UTF-8' })
                //GXP1610
                    .ele('gs_provision', {'version':'1'})
                    .ele({'mac': conf.mac}).up()
                    .ele('config', {'version':'1'})
                    .ele({'P270': conf.name1 + conf.name2}).up()
                    .ele({'P271':'1'}).up()
                    .ele({'P34': conf.pass}).up()
                    .ele({'P35': conf.login}).up()
                    .ele({'P36': conf.login}).up()
                    .ele({'P47': conf.sip_server}).up()
                    .ele({'P2': configgxp.passPhone}).up()
                    .ele({'P64': configgxp.timeZone}).up()
                    .ele({'P31':'1'}).up()
                    .ele({'P194':'2'}).up()
                    .ele({'P285':'1'}).up()
                    .ele({'P8375':'0'}).up()
                    .ele({'P6767':'1'}).up()
                    .ele({'P212':'1'}).up()
                    .ele({'P237': configgxp.servURL}).up()
                    .ele({'P1362':'ru'}).up()
                    .ele({'P192': configgxp.servURL}).up()
                    .ele({'P122':'1'}).up()
                    .ele({'P401':'0'}).up()
                    .ele({'P1684':'0'}).up()
                    .ele({'P1696':'1'}).up()
                    .ele({'P146':'Grandstream' + conf.mac}).up()
                    .up()
                    .up();

                const xmlGXP1610 = GXP1610.end({ prettyPrint: true });
                res.send(xmlGXP1610)
            }

        }

         else if (conf.model === 'GXP1160'){
            const GXP1160 = create({ version: '1.0', encoding: 'UTF-8' })
            // GXP1160
                .ele('gs_provision', {'version':'1'})
                .ele({'mac': conf.mac}).up()
                .ele('config', {'version':'1'})
                .ele({'P2': configgxp.passPhone}).up()
                .ele({'P3': conf.name1 + conf.name2}).up()
                .ele({'P30': configgxp.serverTimeZone}).up()
                .ele({'P31':'1'}).up()
                .ele({'P146':'Grandstream_' + conf.mac}).up()
                .ele({'P144':'1'}).up()
                .ele({'P145':'1'}).up()
                .ele({'P64': configgxp.timeZone}).up()
                .ele({'P34': conf.pass}).up()
                .ele({'P35': conf.login}).up()
                .ele({'P36': conf.login}).up()
                .ele({'P47': conf.sip_server}).up()
                .ele({'P40':'5060'}).up()
                .ele({'P81':'1'}).up()
                .ele({'P32':'10'}).up()
                .ele({'P1362':'ru'}).up()
                .ele({'P103':'0'}).up()
                .ele({'P122':'1'}).up()
                .ele({'P1402':'0'}).up()
                .ele({'P237': configgxp.servURL}).up()
                .ele({'P192': configgxp.servURL}).up()
                .ele({'P194':'2'}).up()
                .ele({'P285':'1'}).up()
                .ele({'P270': conf.name1 + conf.name2}).up()
                .ele({'P271':'1'}).up()
                .ele({'P1348':'0'}).up()
                .ele({'P330':'0'}).up()
                .ele({'P2301':'1'}).up()
                .ele({'P1526':'1'}).up()
                .up()
                .up()

            const xmlGXP1160 = GXP1160.end({ prettyPrint: true });
            res.send(xmlGXP1160)

        } else if (conf.model === 'GXP1400'){
            const GXP1400 = create({ version: '1.0', encoding: 'UTF-8' })
            // GXP1400
                .ele('gs_provision', {'version':'1'})
                .ele({'mac': conf.mac}).up()
                .ele('config', {'version':'1'})
                .ele({'P2': configgxp.passPhone}).up()
                .ele({'P192': configgxp.DefaultServURL}).up()
                .ele({'P237': configgxp.servURL}).up()
                .ele({'P193':'60'}).up()
                .ele({'P238':'0'}).up()
                .ele({'P212':'2'}).up()
                .ele({'P64': configgxp.timeZone}).up()
                // Profile #1
                .ele({'P271':'1'}).up()
                .ele({'P47': conf.sip_server}).up()
                .ele({'P31':'1'}).up()
                .ele({'P32':'3'}).up()
                .ele({'P63':'0'}).up()
                .ele({'P78':'1'}).up()
                .ele({'P2347':'1'}).up()
                .ele({'P57':'18'}).up()
                .ele({'P58':'98'}).up()
                .ele({'P59':'2'}).up()
                .ele({'P60':'8'}).up()
                .ele({'P62':'9'}).up()
                .ele({'P46':'4'}).up()
                .up()
                .up();

            const xmlGXP1400 = GXP1400.end({ prettyPrint: true });
            res.send(xmlGXP1400)

        } else if (conf.model === 'GXP2610'){
            const GXP2610 = create({ version: '1.0', encoding: 'UTF-8' })
            //GXP2610
                .ele('gs_provision', {'version':'1'})
                .ele({'mac': conf.mac}).up()
                .ele('config', {'version':'1'})
                .ele({'P270': conf.name2 + conf.name1}).up()
                .ele({'P271':'1'}).up()
                .ele({'P34': conf.pass}).up()
                .ele({'P35': conf.login}).up()
                .ele({'P36': conf.login}).up()
                .ele({'P47': conf.sip_server}).up()
                .ele({'P2': configgxp.passPhone}).up()
                .ele({'P64': configgxp.timeZone}).up()
                .ele({'P31':'1'}).up()
                .ele({'P194':'2'}).up()
                .ele({'P285':'1'}).up()
                .ele({'P8375':'0'}).up()
                .ele({'P6767':'1'}).up()
                .ele({'P212':'1'}).up()
                .ele({'P237': configgxp.servURL}).up()
                .ele({'P1362':'ru'}).up()
                .ele({'P192': configgxp.servURL}).up()
                .ele({'P122':'1'}).up()
                .ele({'P401':'0'}).up()
                .ele({'P1684':'0'}).up()
                .ele({'P1696':'1'}).up()
                .ele({'P146':'Grandstream_' + conf.mac}).up()
                .up()
                .up();

            const xmlGXP2610 = GXP2610.end({ prettyPrint: true });
            res.send(xmlGXP2610)

        } else next ();
    });
},('/posts', (req, res) => {
    const ip_phone = res.connection.remoteAddress.slice(7);
    const ip_mac = req.url.slice(4,16);
    const date = req._startTime
    const conf = new Conf({
        mac: ip_mac,
        ip_phone: ip_phone ,
        login: '',
        pass: '',
        name1: '',
        name2: '',
        vlan_id: '',
        model: '',
        date: date,
        sip_server: '',
        last_udp: ''
    });
    conf.save((err, data) => {
        if (err) {
            console.log(err)
        } else {
            res.send({
                success: true,
                message: `Post with ID_${data._id} saved successfully!`,
            })
        }
    })
}));

module.exports = router;

const express = require('express');
const router = express.Router();
const Conf = require('../models/conf-model');

// Запись в базу
router.post('/posts', (req, res) => {
  const conf = new Conf({
    mac: req.body.mac,
    ip_phone: req.body.ip_phone,
    login: req.body.login,
    pass: req.body.pass,
    name1: req.body.name1,
    name2: req.body.name2,
    vlan_id: req.body.vlan_id,
    model: req.body.model,
    date: req.body.date,
    sip_server: req.body.sip_server,
    last_udp: req.body.last_udp
  });
  conf.save((err, data) => {
    if (err) {
      console.log(err)
    } else {
      res.send({
        success: true,
        message: `Post with ID_${data._id} saved successfully!`,
      })
    }
  })
 });

// Пулучение из базы
router.get('/posts', (req, res) => {
  Conf.find({}, ' id mac ip_phone login pass name1 name2 vlan_id model date sip_server last_udp', (err, conf) => {
    if (err) {
      res.sendStatus(500)
    } else {
      res.send({ conf: conf })
    }
  }).sort({ _id: -1 })
});

// Получение единичной записи

router.get('/posts/:id', (req, res) => {
  Conf.findById(req.params.id, ' id mac ip_phone login pass name1 name2 vlan_id model date sip_server last_udp', (err, conf) => {
    if (err) {
      res.sendStatus(500)
    } else {
      res.send(conf)
    }
  })
});

// Обновление единичной записи

router.put('/posts/:id', (req, res) => {
  Conf.findById(req.params.id, 'id mac ip_phone login pass name1 name2 vlan_id model date sip_server last_udp', (err, conf) => {
    if (err) {
      console.log(err)
    } else {
      if (req.body.mac) {
        conf.mac = req.body.mac
      }if (req.body.ip_phone) {
        conf.ip_phone = req.body.ip_phone
      }if (req.body.login) {
        conf.login = req.body.login
      }if (req.body.pass) {
        conf.pass = req.body.pass
      }if (req.body.name1) {
        conf.name1 = req.body.name1
      }if (req.body.name2) {
        conf.name2 = req.body.name2
      }if (req.body.vlan_id) {
        conf.vlan_id = req.body.vlan_id
      }if (req.body.model) {
        conf.model = req.body.model
      }if (req.body.sip_server) {
        conf.sip_server = req.body.sip_server
      }if (req.body.date) {
        conf.date = req.body.date
      }if (req.body.last_udp) {
        conf.last_udp = req.body.last_udp
      }
      conf.save(err => {
        if (err) {
          res.sendStatus(500)
        } else {
          res.sendStatus(200)
        }
      })
    }
  })
});

// удаление из базы

router.delete('/posts/:id', (req, res) => {
  Conf.remove({ _id: req.params.id }, err => {
    if (err) {
      res.sendStatus(500)
    } else {
      res.sendStatus(200)
    }
  })
});
module.exports = router;
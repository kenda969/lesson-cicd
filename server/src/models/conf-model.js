const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ConfSchema = new Schema({

  mac: {
    type: String
  },
  ip_phone: {
    type: String
  },
  login: {
    type: String
  },
  pass: {
    type: String
  },
  name1: {
    type: String
  },
  name2: {
    type: String
  },
  vlan_id: {
    type: String
  },
  model: {
    type: String
  },
  date: {
    type: Date
  },
  sip_server: {
    type: String
  },
  last_udp: {
    type: Date
  },
});
const ConfModel = mongoose.model('conf', ConfSchema);
module.exports = ConfModel;
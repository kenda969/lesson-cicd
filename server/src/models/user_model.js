const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  login: {
    type: String
  },
  password: {
    type: String,
    required: true
  }
});
const UserModel = mongoose.model('users', UserSchema);
module.exports = UserModel;
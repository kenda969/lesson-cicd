module.exports = {
  srvport: 8080,
  dbport: 8081,
  dbURL: 'mongodb://mongo:27017/prov',
  dbOptions: { useMongoClient: true },
  asterUser: '',
  asterSecret: '',
  asterServer: '',
  asterPort: ''
};

